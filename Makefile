# SPDX-FileCopyrightText: 2024 jordi fita mas <jfita@peritasoft.com>
# SPDX-License-Identifier: GPL-3.0-or-later

all: bin/xenorogue.rom bin/index.html

bin/%: src/%.c
	mkdir -p $(@D)
	$(LINK.c) $^ $(LOADLIBES) $(LDLIBS) -o $@

bin/%.rom: src/%.tal bin/uxnasm
	bin/uxnasm $< $@

bin/index.html: src/index.html bin/xenorogue.rom
	m4 --prefix-builtins $< > $@

clean:
	$(RM) bin/index.html
	$(RM) bin/*.rom
	$(RM) bin/*.rom.sym
	$(RM) bin/uxnasm

.PHONY: all clean
